package advice;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;

//public class LogAdvice implements MethodBeforeAdvice{
//
//	public void before(Method arg0, Object[] arg1, Object arg2) throws Throwable {
//		System.out.println("---log before---");
//	}
//
//}

public class LogAdvice implements ThrowsAdvice{

	public void afterThrowing(Throwable throwable){
		System.out.println("---log exception----");
	}
}
