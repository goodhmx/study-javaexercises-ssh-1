package auth;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Data access object (DAO) for domain model class Auth.
 * @see auth.Auth
 * @author MyEclipse - Hibernate Tools
 */
public class AuthDAO extends HibernateDaoSupport {

    private static final Log log = LogFactory.getLog(AuthDAO.class);

	//property constants
	public static final String AUTH = "auth";

	protected void initDao() {
		//do nothing
	}
    
    public void save(Auth transientInstance) {
        log.debug("saving Auth instance");
        try {
            getHibernateTemplate().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(Auth persistentInstance) {
        log.debug("deleting Auth instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public Auth findById( java.lang.Integer id) {
        log.debug("getting Auth instance with id: " + id);
        try {
            Auth instance = (Auth) getHibernateTemplate()
                    .get("auth.Auth", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(Auth instance) {
        log.debug("finding Auth instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding Auth instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from Auth as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByAuth(Object auth) {
		return findByProperty(AUTH, auth);
	}
	
    public Auth merge(Auth detachedInstance) {
        log.debug("merging Auth instance");
        try {
            Auth result = (Auth) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(Auth instance) {
        log.debug("attaching dirty Auth instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(Auth instance) {
        log.debug("attaching clean Auth instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static AuthDAO getFromApplicationContext(ApplicationContext ctx) {
    	return (AuthDAO) ctx.getBean("AuthDAO");
	}
}