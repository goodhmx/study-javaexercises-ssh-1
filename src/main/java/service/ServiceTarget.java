package service;

import java.util.Set;
import user_info.*;
import orders.*;
import orderitem.*;
import customer.*;
import card.*;
import users.*;
import auth.*;

public class ServiceTarget implements Service{
	
   private UserInfoDAO userInfoDao;
   private OrdersDAO ordersDao;
   private CustomerDAO customerDAO;
   private CardDAO cardDAO;
   private UsersDAO usersDAO;
   private AuthDAO authDAO;
   
   public void ManyToMany(){
	   //查询
//	   Users user = usersDAO.findById(Integer.valueOf("6"));
//	   System.out.println(user.getName());
//	   java.util.Iterator it = user.getAuth().iterator();
//	   while(it.hasNext()){
//		   Auth auth = (Auth)it.next();
//		   System.out.println(auth.getAuthId());
//		   System.out.println(auth.getAuth());
//		   
//	   }
	   
	   //添加
	   //一个用户有多个权限
	   Users user = new Users();
	   user.setName("wxy");
	   
	   java.util.Set set= new java.util.HashSet();
	   Auth auth = new Auth();
	   auth.setAuth(Integer.valueOf("6666"));
	   set.add(auth);
	   
	   Auth auth1 = new Auth();
	   auth1.setAuth(Integer.valueOf("7777"));
	   set.add(auth1);
	   
	   Auth auth2 = new Auth();
	   auth2.setAuth(Integer.valueOf("8888"));
	   set.add(auth2);
	   
	   user.setAuth(set);
	   usersDAO.save(user);
	   
   }
   public void OneToOne(){
	   //查询
	   Customer customer = customerDAO.findById(Integer.valueOf("11"));
	   System.out.println(customer.getCustomerId());
	   System.out.println(customer.getName());
	   System.out.println(customer.getEmail());
	   Card card = customer.getCard();
	   System.out.println(card.getCardId());
	   System.out.println(card.getCardNo());
	   System.out.println(card.getCardPrice());

	   
//	 添加
//	   Customer customer = new Customer();
//	   customer.setName("qq");
//	   customer.setEmail("qqqsssq@ddd");
//	   
//	   Card card = new Card();
//	   card.setCardNo("qq");
//	   card.setCardPrice(Long.valueOf("1"));
//	   
//	   
//	   card.setCustomer(customer);
//	   customer.setCard(card);
//	   
//	   cardDAO.save(card);
	   
	
	  
   }
  public void addData(UserInfo userInfo){
	 //  userInfoDao.save(userInfo);
	   System.out.println("userInfoDao:"+userInfoDao);
   }
   public void findById(Integer id){
	   UserInfo user = userInfoDao.findById(id);
	   System.out.println(user.getLoginName());
	   System.out.println(user.getUserPassword());
   }
   public void transactionMethod(UserInfo userInfo) throws Exception{

		   userInfoDao.merge(userInfo);
		   System.out.println("---merge suc---");
		   //插入空记录，会异常 
		  // userInfo = new UserInfo();
		   userInfoDao.save(userInfo);

		   System.out.println("---sava suc2---");
		   
			throw new Exception("dddd");
		   
   }
   
   public void hibernateTestMethod(){//一对多
	   //查询
	   System.out.println("hello!");
	   Orders order = ordersDao.findById(Long.valueOf("1"));
	   System.out.println(order.getOrderId());
	   System.out.println(order.getTotalprice());
	   Set set = order.getOrderitem();
	   java.util.Iterator it = set.iterator();
	   while(it.hasNext()){
		   Orderitem item = (Orderitem)it.next();
		   System.out.println(item.getOrderitemId());
		   System.out.println(item.getPrice());
		   System.out.println(item.getOrderId());
	   }
	   //添加
//	   Orders order = new Orders();
//	   order.setTotalprice(Long.valueOf("1100"));
//	   
//	   Orderitem item = new Orderitem();
//	   item.setPrice(Long.valueOf("2100"));
//	   item.setOrders(order);
//	   order.getOrderitem().add(item);
//	   
//	   Orderitem item1 = new Orderitem();
//	   item1.setPrice(Long.valueOf("3100"));
//	   item1.setOrders(order);
//	   order.getOrderitem().add(item1);
//	   
//	   ordersDao.save(order);
   }
   
   
   public OrdersDAO getOrdersDao() {
	return ordersDao;
}
public void setOrdersDao(OrdersDAO ordersDao) {
	this.ordersDao = ordersDao;
}
public UserInfoDAO getUserInfoDao(){
	   return this.userInfoDao;
   }
   public void setUserInfoDao(UserInfoDAO userInfoDao){
	   this.userInfoDao=userInfoDao;
   }
public CustomerDAO getCustomerDAO() {
	return customerDAO;
}
public void setCustomerDAO(CustomerDAO customerDAO) {
	this.customerDAO = customerDAO;
}
public CardDAO getCardDAO() {
	return cardDAO;
}
public void setCardDAO(CardDAO cardDAO) {
	this.cardDAO = cardDAO;
}
public AuthDAO getAuthDAO() {
	return authDAO;
}
public void setAuthDAO(AuthDAO authDAO) {
	this.authDAO = authDAO;
}
public UsersDAO getUsersDAO() {
	return usersDAO;
}
public void setUsersDAO(UsersDAO usersDAO) {
	this.usersDAO = usersDAO;
}
   
}
