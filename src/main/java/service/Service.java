package service;

import user_info.*;

public interface Service {

   public void addData(UserInfo userInfo);
   public void findById(Integer id);
   public void transactionMethod(UserInfo userInfo) throws Exception;
   public void hibernateTestMethod();
   public void OneToOne();
   public void ManyToMany();
}
