package orders;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Data access object (DAO) for domain model class Orders.
 * @see orders.Orders
 * @author MyEclipse - Hibernate Tools
 */
public class OrdersDAO extends HibernateDaoSupport {

    private static final Log log = LogFactory.getLog(OrdersDAO.class);

	//property constants
	public static final String TOTALPRICE = "totalprice";

	protected void initDao() {
		//do nothing
	}
    
    public void save(Orders transientInstance) {
        log.debug("saving Orders instance");
        try {
            getHibernateTemplate().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(Orders persistentInstance) {
        log.debug("deleting Orders instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public Orders findById( java.lang.Long id) {
        log.debug("getting Orders instance with id: " + id);
        try {
            Orders instance = (Orders) getHibernateTemplate()
                    .get("orders.Orders", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(Orders instance) {
        log.debug("finding Orders instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding Orders instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from Orders as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByTotalprice(Object totalprice) {
		return findByProperty(TOTALPRICE, totalprice);
	}
	
    public Orders merge(Orders detachedInstance) {
        log.debug("merging Orders instance");
        try {
            Orders result = (Orders) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(Orders instance) {
        log.debug("attaching dirty Orders instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(Orders instance) {
        log.debug("attaching clean Orders instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static OrdersDAO getFromApplicationContext(ApplicationContext ctx) {
    	return (OrdersDAO) ctx.getBean("OrdersDAO");
	}
}