package usersauth;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Data access object (DAO) for domain model class Usersauth.
 * @see usersauth.Usersauth
 * @author MyEclipse - Hibernate Tools
 */
public class UsersauthDAO extends HibernateDaoSupport {

    private static final Log log = LogFactory.getLog(UsersauthDAO.class);

	//property constants
	public static final String USER_ID = "userId";
	public static final String AUTH_ID = "authId";

	protected void initDao() {
		//do nothing
	}
    
    public void save(Usersauth transientInstance) {
        log.debug("saving Usersauth instance");
        try {
            getHibernateTemplate().save(transientInstance);
            log.debug("save successful");
        } catch (RuntimeException re) {
            log.error("save failed", re);
            throw re;
        }
    }
    
	public void delete(Usersauth persistentInstance) {
        log.debug("deleting Usersauth instance");
        try {
            getHibernateTemplate().delete(persistentInstance);
            log.debug("delete successful");
        } catch (RuntimeException re) {
            log.error("delete failed", re);
            throw re;
        }
    }
    
    public Usersauth findById( java.lang.Integer id) {
        log.debug("getting Usersauth instance with id: " + id);
        try {
            Usersauth instance = (Usersauth) getHibernateTemplate()
                    .get("usersauth.Usersauth", id);
            return instance;
        } catch (RuntimeException re) {
            log.error("get failed", re);
            throw re;
        }
    }
    
    
    public List findByExample(Usersauth instance) {
        log.debug("finding Usersauth instance by example");
        try {
            List results = getHibernateTemplate().findByExample(instance);
            log.debug("find by example successful, result size: " + results.size());
            return results;
        } catch (RuntimeException re) {
            log.error("find by example failed", re);
            throw re;
        }
    }    
    
    public List findByProperty(String propertyName, Object value) {
      log.debug("finding Usersauth instance with property: " + propertyName
            + ", value: " + value);
      try {
         String queryString = "from Usersauth as model where model." 
         						+ propertyName + "= ?";
		 return getHibernateTemplate().find(queryString, value);
      } catch (RuntimeException re) {
         log.error("find by property name failed", re);
         throw re;
      }
	}

	public List findByUserId(Object userId) {
		return findByProperty(USER_ID, userId);
	}
	
	public List findByAuthId(Object authId) {
		return findByProperty(AUTH_ID, authId);
	}
	
    public Usersauth merge(Usersauth detachedInstance) {
        log.debug("merging Usersauth instance");
        try {
            Usersauth result = (Usersauth) getHibernateTemplate()
                    .merge(detachedInstance);
            log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            log.error("merge failed", re);
            throw re;
        }
    }

    public void attachDirty(Usersauth instance) {
        log.debug("attaching dirty Usersauth instance");
        try {
            getHibernateTemplate().saveOrUpdate(instance);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }
    
    public void attachClean(Usersauth instance) {
        log.debug("attaching clean Usersauth instance");
        try {
            getHibernateTemplate().lock(instance, LockMode.NONE);
            log.debug("attach successful");
        } catch (RuntimeException re) {
            log.error("attach failed", re);
            throw re;
        }
    }

	public static UsersauthDAO getFromApplicationContext(ApplicationContext ctx) {
    	return (UsersauthDAO) ctx.getBean("UsersauthDAO");
	}
}